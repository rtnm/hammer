## Hammer

Hammer - это небольшая библиотека для асинхронного ([TAP](http://msdn.microsoft.com/ru-ru/library/hh873175%28v=vs.110%29.aspx)) взаимодействия с базами данных 1С через ComConnector. Может быть использована как основа для взаимодействия с базами данных 1С в многопоточных приложениях, таких например как веб-сервера.

### Hammer решает следующие проблемы:

- Избыточность занятых рабочих потоков которые будут ожидать появления освободившегося соединения в пуле ComConnector-а
`connector.Connect(connectionString)`

- Обязательность освобождения памяти каждого com-объекта с которым происходило взаимодействие
`Marshal.ReleaseComObject(comObject)`

## Строка соединения

Для удобного и простого получения строки соединения с базой данных можно использовать вспомогательную функцию `HHelper.ConnectionStringTo`.

Пример:

```
#!c#

    string server = "server";
    string database = "databese";
    string user = "user";
    string password = "password";
    string connectionString = HHelper.ConnectionStringTo(server, database, user, password);
```

## Планировщик заданий

В основе решения проблемы избыточности запущенных потоков лежит реализация собственного планировщика заданий (task scheduler). Его особенность заключается в том, что он создает ComConnector c определенными значениями PoolCapacity и MaxConnections и планирует выполнение задач (task) так, что для каждой запускаемой задачи выделяется отдельное соединение из пула потоков ComConnector-а.

Пример:

```
#!c#

    using Rtnm.Hammer;
    
    HVersion version = HVersion.V82; //V82.ComConnector или V83.ComConnector
    string connectionString = HHelper.ConnectionStringTo("server", "database", "user", "password");
    int poolCapacity = 3; //размер пула потоков ComConnector-а
    HTaskScheduler ts = new HTaskScheduler(version, connectionString, poolCapacity);
```

## Соединение

При выполнении задачи которая должна взаимодействовать с базой данных 1С первым делом необходимо получить соединение с помощью функции `HHelper.GetConnection` 

Пример:

```
#!c#

    dynamic connection = HHelper.GetConnection();
```

## Освобождение памяти

Для решения проблемы освобождения памяти используется специальная оболочка com-объектов. Для пользователя библиотеки это происходит прозрачным образом.

## Пример

```
#!c#

    static void Main(string[] args)
    {
        string connectionString = HHelper.ConnectionStringTo("server", "database", "user", "password");
        using (HTaskScheduler ts = new HTaskScheduler(HVersion.V82, connectionString, 3))
        {
            Func<object, int> countAndShowProducts = (state) =>
            {
                int taskNumber = (int)state;
                dynamic connection = HHelper.GetConnection();

                dynamic query = connection.newObject("Query");
                query.Text = @"ВЫБРАТЬ
		                            Номенклатура.Код,
		                            Номенклатура.Наименование
		                        ИЗ
		                            Справочник.Номенклатура КАК Номенклатура
		                        ГДЕ
		                            НЕ Номенклатура.ЭтоГруппа";
                dynamic tableOfProducts = query.Execute().Unload();
                for (int i = 0; i < tableOfProducts.Count(); i++)
                {
                    dynamic rowOfProduct = tableOfProducts.Get(i);
                    string code = rowOfProduct.Get(0);
                    string name = rowOfProduct.Get(1);
                    Console.WriteLine("{0}, {1}, {2}", taskNumber, rowOfProduct.Get(0), rowOfProduct.Get(1));
                }
                return tableOfProducts.Count();
            };

            int numberOfTasks = 10;
            TaskFactory<int> tf = new TaskFactory<int>(ts);
            Task<int>[] tasks = new Task<int>[numberOfTasks];
            for (int i = 0; i < numberOfTasks; i++)
            {
                tasks[i] = tf.StartNew(countAndShowProducts, i);
            }
            Task.WaitAll(tasks);
        }
        Console.WriteLine("Все задачи выполнены");
        Console.ReadKey();
    }
```

## Распространение

Библиотека распространяется как Nuget-пакет [Rtnm.Hammer](https://www.nuget.org/packages/Rtnm.Hammer)
