﻿using System;
using System.Threading.Tasks;

namespace Rtnm.Hammer
{
    public class HHelper
    {
        public static string ConnectionStringTo(string server, string database, string user, string password)
        {
            throwExceptionIfNullReference(server, "server");
            throwExceptionIfNullReference(database, "database");
            throwExceptionIfNullReference(user, "user");
            throwExceptionIfNullReference(password, "password");

            return string.Format("Srvr=\"{0}\";Ref=\"{1}\";Usr=\"{2}\";Pwd=\"{3}\";", 
                prepare(server), prepare(database), prepare(user), prepare(password));
        }

        public static string ConnectionStringTo(string catalog, string user, string password)
        {
            throwExceptionIfNullReference(catalog, "server");
            throwExceptionIfNullReference(user, "user");
            throwExceptionIfNullReference(password, "password");

            return string.Format("File=\"{0}\";Usr=\"{1}\";Pwd=\"{2}\";",
                prepare(catalog), prepare(user), prepare(password));
        }

        public static HConnection GetConnection()
        {
            if (TaskScheduler.Current is HTaskScheduler)
            {
                return ((HTaskScheduler)TaskScheduler.Current).GetConnection();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        internal static void throwExceptionIfNullReference(object value, string name)
        {
            if (value == null)
            {
                throw new NullReferenceException(name);
            }
        }

        internal static dynamic getConnector(HVersion version)
        {
            Type v8type = Type.GetTypeFromProgID(version.ToString() + ".ComConnector");
            return Activator.CreateInstance(v8type);
        }

        private static string prepare(string value)
        {
            return value.Replace("\"", "\"\"");
        }
    }
}
