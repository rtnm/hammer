﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Rtnm.Hammer
{
    public class HConnection: HWrapper
    {
        private Dictionary<object, HWrapper> comObjects;

        internal HConnection(object comConnection)
            :base(comConnection, true)
        {
            comObjects = new Dictionary<object, HWrapper>();
            addWrapper(this);
        }

        internal void Dispose()
        {
            if (comObjects != null)
            {
                foreach (KeyValuePair<object, HWrapper> kv in comObjects)
                {
                    Marshal.ReleaseComObject(kv.Key);
                }
            }
            comObjects = null;
        }

        internal HWrapper getWrapper(object comObject)
        {
            HWrapper wrapper;
            if (comObjects.TryGetValue(comObject, out wrapper))
            {
                return wrapper;
            }
            else
            {
                return new HWrapper(comObject, false);
            }
        }

        internal void addWrapper(HWrapper wrapper)
        {
            comObjects.Add(wrapper.comObject, wrapper);
        }
    }
}
