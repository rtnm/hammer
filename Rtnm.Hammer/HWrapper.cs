﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Rtnm.Hammer
{
    public class HWrapper: DynamicObject
    {
        internal object comObject;

        internal HWrapper(object comObject, bool thisIsConnection)
        {
            this.comObject = comObject;
            if (!thisIsConnection)
            {
                HConnection connection = HHelper.GetConnection();
                connection.addWrapper(this);
            }
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] wrappedArgs, out object result)
        {
            return invoke(binder.Name, BindingFlags.InvokeMethod, wrappedArgs, out result);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return invoke(binder.Name, BindingFlags.GetProperty | BindingFlags.GetField, null, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            object result;
            return invoke(binder.Name, BindingFlags.SetProperty | BindingFlags.SetField, new object[] { value }, out result);
        }

        private bool invoke(string name, BindingFlags bindingFlag, object[] args, out object result)
        {
            result = null;
            unwrapArgs(args);
            try
            {
                if (args != null && args.Length > 0)
                {
                    ParameterModifier[] mods = null;
                    mods = new ParameterModifier[] { new ParameterModifier(args.Length) };
                    for (int i = 0; i < args.Length; i++)
                    {
                        mods[0][i] = true;
                    }
                    result = comObject.GetType().InvokeMember(name, bindingFlag, null, comObject, args, mods, null, null);
                }
                else
                {
                    result = comObject.GetType().InvokeMember(name, bindingFlag, null, comObject, args);
                }
                wrapArgs(args);
                if (result != null && Marshal.IsComObject(result))
                {
                    HConnection connection = HHelper.GetConnection();
                    result = connection.getWrapper(result);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void unwrapArgs(object[] args)
        {
            if (args == null)
            {
                return;
            }

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] is HWrapper)
                {
                    args[i] = ((HWrapper)args[i]).comObject;
                }
                else
                {
                    args[i] = args[i];
                }
            }
        }

        private void wrapArgs(object[] args)
        {
            if (args == null)
            {
                return;
            }

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == null)
                {
                    args[i] = null;
                }
                else if (Marshal.IsComObject(args[i]))
                {
                    HConnection connection = HHelper.GetConnection();
                    args[i] = connection.getWrapper(args[i]);
                }
                else
                {
                    args[i] = args[i];
                }
            }
        }
    }
}
