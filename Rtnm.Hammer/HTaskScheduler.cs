﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Rtnm.Hammer
{
    public class HTaskScheduler : TaskScheduler, IDisposable
    {
        [ThreadStatic]
        private static bool currentThreadIsProcessingItems;

        private readonly LinkedList<Task> tasks = new LinkedList<Task>();

        private const int maxPoolCapacity = 10;
        private readonly int poolCapacity;
        private readonly int poolTimeout;
        private int currenConnections = 0;

        private readonly HVersion version;
        private readonly string connectionString;

        private bool disposing = false;

        private dynamic connector;

        [ThreadStatic]
        private static HConnection connection;

        public HTaskScheduler(HVersion version, string connectionString)
            :this(version, connectionString, 3, 0)
        {
        }

        public HTaskScheduler(HVersion version, string connectionString, int poolCapacity)
            : this(version, connectionString, poolCapacity, 0)
        {
        }

        public HTaskScheduler(HVersion version, string connectionString, int poolCapacity, int poolTimeout)
        {
            HHelper.throwExceptionIfNullReference(connectionString, "connectionString");

            if (poolCapacity < 1 || poolCapacity > maxPoolCapacity)
            {
                throw new ArgumentOutOfRangeException("poolCapacity", poolCapacity, "");
            }

            this.version = version;
            this.connectionString = connectionString;
            this.poolCapacity = poolCapacity;
            this.poolTimeout = poolTimeout;

            connect();
        }

        private void connect()
        {
            connector = HHelper.getConnector(version);
            connector.MaxConnections = poolCapacity;
            connector.PoolCapacity = poolCapacity;
            connector.PoolTimeout = poolTimeout;
        }

        protected override void QueueTask(Task task)
        {
            lock (tasks)
            {
                if (disposing)
                {
                    throw new InvalidOperationException();
                }

                tasks.AddLast(task);
                if (currenConnections < poolCapacity)
                {
                    Interlocked.Increment(ref currenConnections);
                    ThreadPool.UnsafeQueueUserWorkItem(worker, null);
                }
            }
        }

        private void worker(object state)
        {
            currentThreadIsProcessingItems = true;
            try
            {
                connection = new HConnection(connector.Connect(connectionString));

                while (true)
                {
                    Task task;
                    lock (tasks)
                    {
                        if (tasks.Count == 0)
                        {
                            break;
                        }
                        task = tasks.First.Value;
                        tasks.RemoveFirst();
                    }
                    base.TryExecuteTask(task);
                }
            }
            finally
            { 
                currentThreadIsProcessingItems = false;
                Interlocked.Decrement(ref currenConnections);
                if (connection != null)
                {
                    connection.Dispose();
                    connection = null;
                }
            }
        }
 
        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            if (!currentThreadIsProcessingItems)
            {
                return false;
            };

            if (taskWasPreviouslyQueued)
            {
                if (TryDequeue(task))
                {
                    return base.TryExecuteTask(task);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return base.TryExecuteTask(task);
            }
        }

        protected override bool TryDequeue(Task task)
        {
            lock (tasks)
            {
                return tasks.Remove(task);
            }
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            bool lockTaken = false;
            try
            {
                Monitor.TryEnter(tasks, ref lockTaken);
                if (lockTaken)
                {
                    return tasks;
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            finally
            {
                if (lockTaken)
                {
                    Monitor.Exit(tasks);
                }
            }
        }

        public HConnection GetConnection()
        {
            return connection;
        }

        public void Dispose()
        {
            if (connector == null)
            {
                return;
            }

            lock (tasks)
            {
                disposing = true;
                tasks.Clear();
            }

            Marshal.ReleaseComObject(connector);
            connector = null;
        }

        public override int MaximumConcurrencyLevel
        {
            get
            {
                return maxPoolCapacity;
            }
        }
    }
}
